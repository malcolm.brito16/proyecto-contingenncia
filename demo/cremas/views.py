from django.shortcuts import render
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from .models import Crema
from .forms import NewCremaForm
# Create your views here.

##### CRUD ARTICULO #####

## Create
## generic.CreateView
class NewCrema(LoginRequiredMixin, generic.CreateView):
    template_name = "cremas/new_cremas.html"
    model = Crema
    context_object_name = "obj"
    form_class = NewCremaForm
    login_url = "home:login"
    success_url = reverse_lazy("cremas:list_cremas")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


## Retrieve
# generic.ListView
class ListCremas(LoginRequiredMixin, generic.ListView):
    template_name = "productos/list_produc.html"
    queryset = Cremas.objects.all()
    login_url = "home:login"
    context_object_name = "obj"

## Update

class UpdateCremas(LoginRequiredMixin, generic.UpdateView):
    template_name = "Cremas/new_cremas.html"
    model = Crema
    context_object_name = "obj"
    form_class = NewCremaForm
    login_url = "home:login"
    success_url = reverse_lazy("cremas:list_cremas")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)
## Delete
