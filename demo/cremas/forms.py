from django import forms

from .models import cremas


class NewCremaForm(forms.ModelForm):
    class Meta:
        model = Productos
        fields = [
            "producto",
            "categoria",
            "precio",
        ]
        labels = {
            "producto": "Nombre del producto",
            "categoria": "categoria",
            "precio": "Precio"
        }
        widget = {
            "producto": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })
