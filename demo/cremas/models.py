from django.db import models

from productos.models import BaseModel,categoria,crema

# Create your models here.

class Crema(BaseModel):
    producto = models.CharField(max_length=128, unique=True)
    categoria = models.ForeignKey(categoria, on_delete=models.CASCADE)
    precio = models.ForeignKey(precio, on_delete=models.CASCADE)



    def __str__(self):
        return "{}".format(self.description)


    def save(self):
        self.description = self.description.upper()
        super(Producto, self).save()


    class  Meta:
        verbose_name_plural = "Cremas"
