from django.urls import path

from cremas import views

app_name = "cremas"

urlpatterns =[
    path('list_cremas/', views.ListCrema.as_view(), name="list_cremas"),
    path('new_cremas/', views.NewCrema.as_view(), name="new_cremas"),
]
