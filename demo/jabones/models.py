from django.db import models
from django.contrib.auth.models import User



# Create your models here.

class BrandQuerySet(models.QuerySet):
    def old(self):
        return self.filter(id__lt=3)


class BaseModel(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)
    user_update = models.IntegerField(blank=True, null=True)


    class Meta:
        abstract = True


class Category(BaseModel):
    description = models.CharField(
        max_length = 128,
        help_text = "Category Description",
        unique = True
    )

    def __str__(self):
        return "{}".format(self.description)

    def save(self):
        self.description = self.description.upper()
        super(Category, self).save()

    class Meta:
        verbose_name_plural = "Categories"


##### PRODUCT MODEL #####

class Jabon(BaseModel):
    code = models.CharField(max_length=16, unique=True)
    description = models.CharField(max_length=128)
    Precio = models.FloatField(default=0)



    def __str__(self):
        return "{}".format(self.description)





    class Meta:
        verbose_name_plural = "Jabones"
