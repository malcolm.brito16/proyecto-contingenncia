from django import forms

from .models import Category, Jabon

class NewCategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = [
            "description",
            "status",
        ]
        labels = {
            "description": "Category",
            "status": "Status"
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })
class NewJabonForm(forms.ModelForm):
    class Meta:
        model = Jabon
        fields = [
            "Jabon",
        ]
        labels = {
            "Jabon": "Jabon",
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })
            self.fields["category"].empty_label = "Choose Category"
