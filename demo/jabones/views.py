from django.shortcuts import render
from django.urls import reverse_lazy

from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin

from django.http import JsonResponse, HttpResponse
from django.core import serializers

from .models import Category, Jabon
from .forms import NewCategoryForm, NewJabonForm
# Create your views here.


##### CRUD Category #####


##### RETRIEVE (List, Detail) #####
## generic.ListView
class ListCategory(LoginRequiredMixin, generic.ListView):
    template_name = "productos/list_category.html"
    model = Category
    context_object_name = "obj"
    login_url = "home:login"

## generic.DetailView
class DetailCategory(LoginRequiredMixin, generic.DetailView):
    template_name = "productos/detail_category.html"
    model = Category
    login_url = "home:login"
    context_object_name = "obj"

class NewCategory(LoginRequiredMixin, generic.CreateView):
    template_name = "prodocutos/new_category.html"
    model = Category
    context_object_name = "obj"
    form_class = NewCategoryForm
    login_url = "home:login"
    success_url = reverse_lazy("productos:list_category")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

##### UPDATE #####
## generic.UpdateView
class UpdateCategory(LoginRequiredMixin, generic.UpdateView):
    template_name = "productos/update_category.html"
    model = Category
    context_object_name = "obj"
    form_class = NewCategoryForm
    login_url = "home:login"
    success_url = reverse_lazy("pinturas:list_category")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)



##### DELETE #####
## generic.DeleteView
class DeleteCategory(LoginRequiredMixin, generic.DeleteView):
    template_name = "productos/delete_category.html"
    model = Category
    context_object_name = "obj"
    login_url = "home:login"
    success_url = reverse_lazy("productos:list_category")


##### RETRIEVE #####
## generic.ListView
class ListJabon(LoginRequiredMixin, generic.ListView):
    template_name = "productos/list_jabon.html"
    model = Jabon
    context_object_name = "obj"
    login_url = "home:login"


class DetailJabon(LoginRequiredMixin, generic.DetailView):
    template_name = "productos/detail_jabon.html"
    model = Era
    login_url = "home:login"
    context_object_name = "obj"


##### UPDATE #####
## generic.UpdateView
class UpdateJabon(LoginRequiredMixin, generic.UpdateView):
    template_name = "productos/update_jabon.html"
    model = Jabon
    context_object_name = "obj"
    form_class = NewJabonForm
    login_url = "home:login"
    success_url = reverse_lazy("productos:list_jabon")


    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

##### DELETE #####
## generic.DeleteView
class Delete(LoginRequiredMixin, generic.DeleteView):
    template_name = "productos/delete_jabon.html"
    model = Jabon
    context_object_name = "obj"
    success_url = reverse_lazy("productos:list_jabon")
    login_url = "home:login"


##### API's #####
## ws JSON

def wsList(request):
    data = serializers.serialize('json', Category.objects.filter(status=False))
    return HttpResponse(data, content_type="application/json")
