from django.urls import path

from pinturas import views

app_name = "jabones"

urlpatterns = [
    path('list_category/', views.ListCategory.as_view(), name="list_category"),
    path('new_category/', views.NewCategory.as_view(), name="new_category"),
    path('update_category/<int:pk>/', views.UpdateCategory.as_view(), name="update_category"),
    path('delete_category/<int:pk>/', views.DeleteCategory.as_view(), name="delete_category"),
    path('new_jabon/', views.NewJabon.as_view(), name="new_jabon"),
    path('list_jabon/', views.ListJabon.as_view(), name="list_jabon"),
    path('update_jabon/<int:pk>/', views.UpdateJabon.as_view(), name="update_jabon"),
    path('delete_jabon/<int:pk>/', views.DeleteJabon.as_view(), name="delete_jabon"),
    path('detail_category/<int:pk>/', views.DetailCategory.as_view(), name="detail_category"),
    path('detail_jabon/<int:pk>/', views.DetailJabon.as_view(), name="detail_jabon"),
    path('ws/list_category/', views.wsList, name="ws_list_category"),

]
